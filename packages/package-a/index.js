import moment from "moment";

export const getCurrentDateFormatted = () => {
  return moment().format("LLLL");
};
