import { getCurrentDateFormatted } from "../package-a";

export const getFormattedDateWithLabel = (label = "Today is:") => {
  return `${label}${getCurrentDateFormatted()}`;
};
